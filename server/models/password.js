// Dependencies 
var restful = require('node-restful');
var mongoose = restful.mongoose;

// Schema
var passwordSchema = new mongoose.Schema({
	site: String, 
	password: String,
	category: String,
	hash: String
});

// Return Model
module.exports = restful.model('Passwords', passwordSchema);