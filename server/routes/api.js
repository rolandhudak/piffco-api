// Dependencies
var express = require('express');
var jwt = require('express-jwt');
var router = express.Router();

require('../config/passport');

// Middleware
var auth = jwt({secret: 'SECRET', userProperty: 'payload'});


// Models
var PiffcoPassword = require('../models/password');
var User = require('../models/Users');


// Routes
PiffcoPassword.methods(['get', 'put', 'post', 'delete']);
// PiffcoPassword.before('post', info);
PiffcoPassword.register(router, '/passwords');

router.post('/register', function(req, res, next){
  if(!req.body.username || !req.body.password){
    return res.status(400).json({message: 'Please fill out all fields'});
  }

  var user = new User();

  user.username = req.body.username;

  user.setPassword(req.body.password)

  user.save(function (err){
    if(err){ return next(err); }

    return res.json({token: user.generateJWT()})
  });
});

router.post('/login', function(req, res, next){
  if(!req.body.username || !req.body.password){
    return res.status(400).json({message: 'Please fill out all fields'});
  }

  passport.authenticate('local', function(err, user, info){
    if(err){ return next(err); }

    if(user){
      return res.json({token: user.generateJWT()});
    } else {
      return res.status(401).json(info);
    }
  })(req, res, next);
});



function info(req, res, next) {
	console.log('before attempt to route');
	next();
}

// Return Router
module.exports = router;
