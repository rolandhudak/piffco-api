// Dependencies
var express = require('express');
var mongoose = require('mongoose');
var passport = require('passport');
var bodyParser = require('body-parser');

// MongoDB connect
mongoose.connect('mongodb://localhost/piffco-dev', function() {
	console.log('Piffco is connected to piffco-dev database.');
});

// Express setup
var app = express();
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());

// JWT setup
require('./models/Users');
require('./config/passport');
app.use(passport.initialize());

// Router
app.use('/api', require('./routes/api'));

//  Starting up the server
app.listen(8000, function() {
	console.log('Piffco is running on port 8000.');
});